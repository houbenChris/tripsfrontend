import {OnInit, Input, Component} from "@angular/core";
import {Trip} from "../model/trip.type";
import {TripService} from "../services/trip.service";

@Component({
  selector: 'search-trip-component',
  template: `
<table class="ui celled table">
  <thead>
    <tr>
    <th>Name</th>
    <th>Start date</th>
    <th>Start Location</th>
    <th>End Location</th>
  </tr></thead>
  <tbody>
    <tr *ngFor="let trip of trips">
    <td  (click)="onSelect(trip)">{{trip.name}}</td>
    <td>{{trip.startDate}}</td>
    <td>{{trip.startLocation.name}}</td>
    <td>{{trip.stopLocation.name}}</td>
    </tr>
  </tbody>

</table>

<div *ngIf="selectedTrip">
  <h2>Details van trip: {{selectedTrip.name}}</h2>
 
   <div>
    <table class="ui celled table">
    <thead >
    <tr>
    <th>Stopover Locations:</th>
    
</tr>
</thead>
<tbody>

<tr *ngFor="let stopovers of selectedTrip.stopovers">
<td>
{{stopovers.name}}
</td>
</tr> 
</tbody>
<thead>
<tr><th>Trip description:</th></tr>
</thead>
<tbody>
<tr>
<td>{{selectedTrip.description}}</td>
</tr>
</tbody>
</table>
  </div>
</div>
`
})

export class SearchTripComponent implements OnInit {
  private trips: Trip[];
  selectedTrip: Trip;

  constructor(private tripService: TripService) {
  }

  ngOnInit() {
    this.tripService.getTrips().subscribe(
      result => {
        this.trips = result;
        console.log(this.trips);

      },
      error => {
        console.log(error as string);
      }
    );
  }

  onSelect(trip: Trip): void {
    this.selectedTrip = trip;
  }

}
