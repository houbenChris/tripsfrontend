export class User {
  name: string;
  birthdate: string;
  email: string;
  password: string;
}
