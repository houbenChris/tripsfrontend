import {Location} from "./location.type";
import {Label} from "./label.type";
import {User} from "./user.type";
export class Trip {
  name : string;
  description:string;
  startLocation: Location;
  stopLocation:Location;
  stopovers:Array<Location>;
  startDate:string;
  labels:Array<Label>;
  isStarted:boolean;
  users:Array<User>;
}
