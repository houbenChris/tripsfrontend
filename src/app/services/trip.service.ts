import { Injectable }     from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Trip }           from '../model/trip.type';
import {Observable} from 'rxjs/Rx';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {XhrBaseRequestOptions, SubscribeResultHandler} from "../util/utils";
@Injectable()
export class TripService {
  private projectRequestMapping: string = "/api/trip";
  private getAllTrips: string = "/findAll";
  private getTripDetails: string="/findByName";
  constructor(private http: Http,
              private xhrBaseRequestOptions: XhrBaseRequestOptions,
              private subscribeResultHandler: SubscribeResultHandler) {
  }
  public getTrips(): Observable<Trip[]> {
    let callUrl: string = this.projectRequestMapping + this.getAllTrips;
    return this.http.get(callUrl, this.xhrBaseRequestOptions)
      .map((res: Response) => this.extractData(res))
      .catch(this.subscribeResultHandler.handleError);
  }

  private extractData(res: Response) {
    let body = <Trip[]> res.json().trips;    // return array from json file
    return body || [];     // also return empty array if there is no data
  }
  public getAllTripDetails(name: string): Observable<Trip>{
    let callUrl: string = this.projectRequestMapping + this.getTripDetails;
    return this.http.get(callUrl, this.xhrBaseRequestOptions)
      .map((res: Response) => this.extractData(res))
      .catch(this.subscribeResultHandler.handleError);

}
}

