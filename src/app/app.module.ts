import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {TripService} from "./services/trip.service";
import {SearchTripComponent} from "./components/search-trip-component";
import {HttpModule} from "@angular/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SubscribeResultHandler, XhrBaseRequestOptions} from "./util/utils";
import {NgSemanticModule} from "ng-semantic/ng-semantic";

@NgModule({
  declarations: [
    AppComponent,
    SearchTripComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    NgSemanticModule,
  ],
  providers: [TripService, SubscribeResultHandler, XhrBaseRequestOptions],
  bootstrap: [AppComponent]
})
export class AppModule { }
